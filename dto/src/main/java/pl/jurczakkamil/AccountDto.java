package pl.jurczakkamil;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.jurczakkamil.consts.AccountType;

import java.math.BigDecimal;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AccountDto extends BaseDto {

    private AccountType type;
    private String name;
    private BigDecimal initialBalance;
    private String icon;
    private String color;
}
