package pl.jurczakkamil;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CategoryDto extends BaseDto {

    private String name;
    private String icon;
    private String color;
}
