package pl.jurczakkamil.converter;

import pl.jurczakkamil.consts.AccountType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class AccountTypeConverter implements AttributeConverter<AccountType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(AccountType accountType) {
        return accountType.getIndex();
    }

    @Override
    public AccountType convertToEntityAttribute(Integer dbData) {
        return AccountType.byIndex(dbData);
    }
}
