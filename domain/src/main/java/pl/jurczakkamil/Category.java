package pl.jurczakkamil;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Category extends BaseEntity {

    @Column(unique = true, nullable = false)
    private String name;
    private String icon;
    private String color;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof final Category that)) return false;
        if (!super.equals(o)) return false;

        if (!name.equals(that.name)) return false;
        if (!Objects.equals(icon, that.icon)) return false;
        return Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
