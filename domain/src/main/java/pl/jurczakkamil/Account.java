package pl.jurczakkamil;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.jurczakkamil.consts.AccountType;
import pl.jurczakkamil.converter.AccountTypeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Account extends BaseEntity {

    @Convert(converter = AccountTypeConverter.class)
    private AccountType type;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    @Digits(integer = 9, fraction = 2, message = "Initial balance should has max 9 digits before comma and max 2 digits after comma")
    private BigDecimal initialBalance;

    private String icon;
    private String color;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof final Account that)) return false;
        if (!super.equals(o)) return false;

        if (type != that.type) return false;
        if (!name.equals(that.name)) return false;
        if (!initialBalance.equals(that.initialBalance)) return false;
        if (!Objects.equals(icon, that.icon)) return false;
        return Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + initialBalance.hashCode();
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
