package pl.jurczakkamil.mapper;

import org.mapstruct.Mapper;
import pl.jurczakkamil.Account;
import pl.jurczakkamil.AccountDto;
import pl.jurczakkamil.model.AccountModel;
import pl.jurczakkamil.service.dictionary.request.AddAccountRequestModel;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    AccountDto toDto(Account account);

    Account toEntity(AccountDto dto);

    AccountModel toCdmObject(AccountDto dto);

    AccountDto toDto(AddAccountRequestModel requestModel);
}
