package pl.jurczakkamil.mapper;

import org.mapstruct.Mapper;
import pl.jurczakkamil.Category;
import pl.jurczakkamil.CategoryDto;
import pl.jurczakkamil.model.CategoryModel;
import pl.jurczakkamil.service.dictionary.request.AddCategoryRequestModel;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    CategoryDto toDto(Category category);

    Category toEntity(CategoryDto dto);

    CategoryModel toCdmObject(CategoryDto dto);

    CategoryDto toDto(AddCategoryRequestModel requestModel);
}
