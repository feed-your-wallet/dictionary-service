package pl.jurczakkamil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jurczakkamil.mapper.CategoryMapper;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.StreamSupport;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    public CategoryDto save(CategoryDto categoryDto) {
        final var category = categoryMapper.toEntity(categoryDto);
        final var saved = categoryRepository.save(category);
        return categoryMapper.toDto(saved);
    }

    public List<CategoryDto> saveAll(List<CategoryDto> categoryDtos) {
        final var categories = categoryDtos.stream()
                .map(categoryMapper::toEntity)
                .toList();
        final var iterable = categoryRepository.saveAll(categories);
        return toCategoryDtos(iterable);
    }

    public CategoryDto findById(Long id) {
        final var category = categoryRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Cannot find Category with ID: " + id));
        return categoryMapper.toDto(category);
    }

    public List<CategoryDto> findAllById(List<Long> ids) {
        final var iterable = categoryRepository.findAllById(ids);
        return toCategoryDtos(iterable);
    }

    public List<CategoryDto> findAll() {
        final var iterable = categoryRepository.findAll();
        return toCategoryDtos(iterable);
    }

    private List<CategoryDto> toCategoryDtos(Iterable<Category> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(categoryMapper::toDto)
                .toList();
    }
}
