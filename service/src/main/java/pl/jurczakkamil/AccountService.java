package pl.jurczakkamil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jurczakkamil.mapper.AccountMapper;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.StreamSupport;

@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Autowired
    public AccountService(AccountRepository accountRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
    }

    public AccountDto save(AccountDto accountDto) {
        final var account = accountMapper.toEntity(accountDto);
        final var saved = accountRepository.save(account);
        return accountMapper.toDto(saved);
    }

    public List<AccountDto> saveAll(List<AccountDto> accountDtos) {
        final var accounts = accountDtos.stream()
                .map(accountMapper::toEntity)
                .toList();
        final var saved = accountRepository.saveAll(accounts);
        return toAccountDtos(saved);
    }

    public AccountDto findById(Long id) {
        final var category = accountRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Cannot find Account with ID: " + id));
        return accountMapper.toDto(category);
    }

    public List<AccountDto> findAllById(List<Long> ids) {
        final var iterable = accountRepository.findAllById(ids);
        return toAccountDtos(iterable);
    }

    public List<AccountDto> findAll() {
        final var iterable = accountRepository.findAll();
        return toAccountDtos(iterable);
    }

    private List<AccountDto> toAccountDtos(Iterable<Account> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(accountMapper::toDto)
                .toList();
    }
}
