package pl.jurczakkamil.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.jurczakkamil.BaseDto;
import pl.jurczakkamil.CategoryDto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

@NoArgsConstructor(access = AccessLevel.NONE)
public class CategoryHelper {

    public static CategoryDto createCategoryDto() {
        final var categoryDto = new CategoryDto();
        categoryDto.setName("Category " + UUID.randomUUID());
        categoryDto.setIcon("test-icon");
        categoryDto.setColor("#282e54");

        return categoryDto;
    }

    public static List<CategoryDto> createCategoryDtoList(int size) {
        final var dtos = new ArrayList<CategoryDto>();
        IntStream.range(0, size)
                .forEach(i -> {
                    final var dto = createCategoryDto();
                    dtos.add(dto);
                });
        return dtos;
    }

    public static List<Long> extractIds(List<CategoryDto> categoryDtos) {
        return categoryDtos.stream()
                .map(BaseDto::getId)
                .toList();
    }
}
