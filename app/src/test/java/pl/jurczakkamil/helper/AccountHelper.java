package pl.jurczakkamil.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.jurczakkamil.AccountDto;
import pl.jurczakkamil.BaseDto;
import pl.jurczakkamil.consts.AccountType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

@NoArgsConstructor(access = AccessLevel.NONE)
public class AccountHelper {

    public static AccountDto createAccountDto() {
        final var accountDto = new AccountDto();
        accountDto.setType(AccountType.MAIN);
        accountDto.setName("Account " + UUID.randomUUID());
        accountDto.setInitialBalance(new BigDecimal("0.0"));
        accountDto.setIcon("test-icon");
        accountDto.setColor("#282e54");

        return accountDto;
    }

    public static List<AccountDto> createAccountDtos(int size) {
        final var dtos = new ArrayList<AccountDto>();
        IntStream.range(0, size)
                .forEach(i -> {
                    final var dto = createAccountDto();
                    dtos.add(dto);
                });
        return dtos;
    }

    public static List<Long> extractIds(List<AccountDto> accountDtos) {
        return accountDtos.stream()
                .map(BaseDto::getId)
                .toList();
    }
}
