package pl.jurczakkamil;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static pl.jurczakkamil.helper.AccountHelper.*;

@SpringBootTest
class AccountServiceTests {

    @Autowired
    private AccountService accountService;

    @Test
    void contextLoads() {
        assertNotNull(accountService);
    }

    @Test
    void givenDto_whenSaveAll_thenPersistsAllInDB() {
        final var dtos = createAccountDtos(5);

        final var saved = accountService.saveAll(dtos);
        final var ids = extractIds(saved);

        final var found = accountService.findAllById(ids);
        assertEquals(5, found.size());
    }

    @Test
    void givenDto_whenSave_thenPersistsInDB() {
        final var dto = createAccountDto();

        final var saved = accountService.save(dto);

        final var found = accountService.findById(saved.getId());
        assertNotNull(found);
    }

    @Test
    void givenListOfDto_whenFindAll_thenReturnsCorrectCollection() {
        final var accounts = createAccountDtos(5);
        final var saved = accountService.saveAll(accounts);
        final var ids = extractIds(saved);

        final var found = accountService.findAllById(ids);

        assertEquals(5, found.size());
    }
}
