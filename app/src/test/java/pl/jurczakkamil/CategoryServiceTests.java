package pl.jurczakkamil;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static pl.jurczakkamil.helper.CategoryHelper.*;

@SpringBootTest
class CategoryServiceTests {

    @Autowired
    private CategoryService categoryService;

    @Test
    void contextLoads() {
        assertNotNull(categoryService);
    }

    @Test
    void givenDto_whenSaveAll_thenPersistsAllInDB() {
        final var dtos = createCategoryDtoList(5);

        final var saved = categoryService.saveAll(dtos);
        final var ids = extractIds(saved);

        final var found = categoryService.findAllById(ids);
        assertEquals(5, found.size());
    }

    @Test
    void givenDto_whenSave_thenPersistsInDB() {
        final var dto = createCategoryDto();

        final var saved = categoryService.save(dto);

        final var found = categoryService.findById(saved.getId());
        assertNotNull(found);
    }

    @Test
    void givenListOfDto_whenFindAll_thenReturnsCorrectCollection() {
        final var accounts = createCategoryDtoList(5);
        final var saved = categoryService.saveAll(accounts);
        final var ids = extractIds(saved);

        final var found = categoryService.findAllById(ids);

        assertEquals(5, found.size());
    }
}
