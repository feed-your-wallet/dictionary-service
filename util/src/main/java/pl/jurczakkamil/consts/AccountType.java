package pl.jurczakkamil.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.NoSuchElementException;

@AllArgsConstructor
@Getter
public enum AccountType {

    MAIN(1),
    CASH(2),
    BANK_ACCOUNT(3),
    SAVINGS_ACCOUNT(4);


    private int index;

    public static AccountType byIndex(int index) {
        return Arrays.stream(AccountType.values())
                .filter(e -> e.getIndex() == index)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Cannot resolve AccountType by index: " + index));
    }
}
