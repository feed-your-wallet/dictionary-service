package pl.jurczakkamil.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.CategoryService;
import pl.jurczakkamil.mapper.CategoryMapper;
import pl.jurczakkamil.service.dictionary.response.CategoriesResponseModel;

@Component
public class GetAllCategoriesRoute extends RouteBuilder {

    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;

    @Autowired
    public GetAllCategoriesRoute(CategoryService categoryService, CategoryMapper categoryMapper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public void configure() {
        from("direct:get-all-categories").id("get-all-categories-route")
                .log(">>> ${body}")
                .setBody(this::findAll);
    }

    private CategoriesResponseModel findAll(Exchange exchange) {
        final var dtos = categoryService.findAll();
        final var categoryModels = dtos.stream()
                .map(categoryMapper::toCdmObject)
                .toList();
        return new CategoriesResponseModel(categoryModels);
    }
}
