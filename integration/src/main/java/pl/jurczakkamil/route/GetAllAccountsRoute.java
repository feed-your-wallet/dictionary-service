package pl.jurczakkamil.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.AccountService;
import pl.jurczakkamil.mapper.AccountMapper;
import pl.jurczakkamil.service.dictionary.response.AccountsResponseModel;

@Component
public class GetAllAccountsRoute extends RouteBuilder {

    private final AccountService accountService;
    private final AccountMapper accountMapper;

    @Autowired
    public GetAllAccountsRoute(AccountService accountService, AccountMapper accountMapper) {
        this.accountService = accountService;
        this.accountMapper = accountMapper;
    }

    @Override
    public void configure() {
        from("direct:get-all-accounts").id("get-all-accounts-route")
                .log(">>> ${body}")
                .setBody(this::findAll);
    }

    private AccountsResponseModel findAll(Exchange exchange) {
        final var dtos = accountService.findAll();
        final var accountModels = dtos.stream()
                .map(accountMapper::toCdmObject)
                .toList();
        return new AccountsResponseModel(accountModels);
    }
}
