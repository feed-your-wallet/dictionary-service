package pl.jurczakkamil.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.AccountService;
import pl.jurczakkamil.mapper.AccountMapper;
import pl.jurczakkamil.service.dictionary.request.AddAccountRequestModel;

@Component
public class AddAccountRoute extends RouteBuilder {

    private final AccountService accountService;
    private final AccountMapper accountMapper;

    @Autowired
    public AddAccountRoute(AccountService accountService, AccountMapper accountMapper) {
        this.accountService = accountService;
        this.accountMapper = accountMapper;
    }

    @Override
    public void configure() {
        from("direct:add-account").id("add-account-route")
                .log(">>> ${body}")
                .process(this::saveAccount)
                .setBody(exchange -> "Successfully saved category");
    }

    private void saveAccount(Exchange exchange) {
        final var requestModel = exchange.getIn().getBody(AddAccountRequestModel.class);
        final var entryDto = accountMapper.toDto(requestModel);
        accountService.save(entryDto);
    }
}
