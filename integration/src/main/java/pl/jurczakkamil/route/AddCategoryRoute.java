package pl.jurczakkamil.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.CategoryService;
import pl.jurczakkamil.mapper.CategoryMapper;
import pl.jurczakkamil.service.dictionary.request.AddCategoryRequestModel;

@Component
public class AddCategoryRoute extends RouteBuilder {

    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;

    @Autowired
    public AddCategoryRoute(CategoryService categoryService, CategoryMapper categoryMapper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public void configure() {
        from("direct:add-category").id("add-category-route")
                .log(">>> ${body}")
                .process(this::saveCategory)
                .setBody(exchange -> "Successfully saved category");
    }

    private void saveCategory(Exchange exchange) {
        final var requestModel = exchange.getIn().getBody(AddCategoryRequestModel.class);
        final var entryDto = categoryMapper.toDto(requestModel);
        categoryService.save(entryDto);
    }
}
