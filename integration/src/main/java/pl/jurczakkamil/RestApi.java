package pl.jurczakkamil;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.service.dictionary.request.AddAccountRequestModel;
import pl.jurczakkamil.service.dictionary.request.AddCategoryRequestModel;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Component
public class RestApi extends RouteBuilder {

    @Override
    public void configure() {

        restConfiguration()
                .contextPath("{{server.servlet.context-path}}")
                .port("{{server.port}}")
                .enableCORS(true)
                .apiContextPath("{{server.servlet.context-path}}")
                .apiProperty("api.title", "{{spring.application.name}}")
                .apiProperty("api.version", "{{application.version}}")
                .apiContextRouteId("doc-dictionary-service-api")
                .component("servlet")
                .bindingMode(RestBindingMode.json);

        rest("/api")
                .get("/hello").consumes(APPLICATION_JSON)
                .to("direct:hello")
                .get("/accounts").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .to("direct:get-all-accounts")
                .post("/accounts").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .type(AddAccountRequestModel.class)
                .to("direct:add-account")
                .get("/categories").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .to("direct:get-all-categories")
                .post("/categories").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .type(AddCategoryRequestModel.class)
                .to("direct:add-category");
    }
}
